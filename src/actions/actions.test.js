export { postMessage, getMessage } from './contactActions';
export { fetchWeather } from "./index";
import { expect } from "../test_helper";
import chai  from "chai";
import spies from 'chai-spies';
import axios from "axios";

describe("Actions", () => {
    const URL_HEROKU_API = "http://reduxblog.herokuapp.com/";
    const API_KEY = "afe3ee0c3173e1890785f28e032102a";
    const ENDPOINTS = {
    POSTS: "api/posts"
    };

    const POST_MESSAGE = "POST_MESSAGE";
    const GET_MESSAGE = "GET_MESSAGE";
    const axios = {
        post: function(body,callback) {
            return new Promise((resolve, reject) => {
                resolve("data");
            })
        }
    }

    it("postMessage should call axios", (postMessage) => {
        chai.spy.on(axios, "post");
        postMessage();
        expect(axios.post).to.have.been.called;
    });

    it("getMessage should call axios", (getMessage) => {
        chai.spy.on(axios, "get");
        getMessage();
        expect(axios.get).to.have.been.called;
    });

    it("fetchWeather should call axios", (fetchWeather) => {
        // const spy = chai.spy.on(axios, "get");
        fetchWeather();
        expect(axios.get).to.have.been.called;
    });
});



