import axios from "axios";

const URL_HEROKU_API = "http://reduxblog.herokuapp.com/";
const API_KEY = "afe3ee0c3173e1890785f28e032102a";
const ENDPOINTS = {
  POSTS: "api/posts"
};

export const POST_MESSAGE = "POST_MESSAGE";
export const GET_MESSAGE = "GET_MESSAGE";

export function postMessage(body, callback) {
  const url = `${URL_HEROKU_API}${ENDPOINTS.POSTS}?key=${API_KEY}`;
  const request = axios.post(url, body);
  request.then(response => {
    callback();
  });

  return {
    type: POST_MESSAGE,
    payload: request
  };
}

export function getMessages() {
  const url = `${URL_HEROKU_API}${ENDPOINTS.POSTS}?key=${API_KEY}`;
  const request = axios.get(url);

  return {
    type: GET_MESSAGE,
    payload: request
  };
}
