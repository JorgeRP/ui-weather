import axios from 'axios';

const API_KEY = 'e19f346caeea76c58c647db73822fc3d';
const URL_ROOT = `http://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}`;
const unit = 'metric';

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${URL_ROOT}&q=${city}&units=${unit}`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
