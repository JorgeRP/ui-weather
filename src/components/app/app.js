import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import WeatherComponent from '../weatherComponent/WeatherComponent';
import ContactComponent from '../contactComponent/contactComponent';
import CommentComponent from '../commentComponent/commentComponent';
import Toolbar from '../toolbar/toolbar';

export default class App extends Component  {
    render() {
        return(
      <div>
        <Toolbar />
        <div>
          <Switch>
            <Route path="/contact" component={ContactComponent} />
            <Route path="/coments" component={CommentComponent} />
            <Route path="/" component={WeatherComponent} />
          </Switch>
        </div>
      </div>
        )
    }
}