import React, { Component } from "react";
import "./commentComponent.css";
import CommentList from "../../containers/commentList/commentList";

export default class CommentComponent extends Component {
  render() {
    return (
      <div className="commentComponent">
        <div>
          <CommentList />
        </div>
      </div>
    );
  }
}
