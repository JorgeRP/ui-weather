import { renderComponent, expect } from "../../test_helper";
import CommentComponent from "./commentComponent";

describe("CommentComponent", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(CommentComponent);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});
