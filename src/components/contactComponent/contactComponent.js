import React, { Component } from 'react';
import './contactComponent.css';
import PostForm from '../../containers/postForm/postForm';

export default class ContactComponent extends Component {
  render() {
    return (
      <div className="contactComponent">
        <div>
          <PostForm />
        </div>
      </div>
    )
  }
}
