import { renderComponent, expect } from "../../test_helper";
import ContactComponent from "./contactComponent";

describe("ContactComponent", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(ContactComponent);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});