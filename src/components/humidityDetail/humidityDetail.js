import React, { Component } from 'react';
import './humidityDetail.css';

export default class HumidityDetail extends Component {
  render() {
    return (
      <div>
        <p className="title">Humidity</p>
        <p className="percent">{this.props.humidity} %</p>
      </div>
    )
  }
}
