import { renderComponent, expect } from "../../test_helper";
import HumidityDetail from "./humidityDetail";

describe("HumidityDetail", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(HumidityDetail);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});
