import React, { Component } from 'react';
import './temperatureDetail.css';

export default class TemperatureDetail extends Component {
  render() {
    return (
      <div>
        <div className="current-temp">
          <p>{this.props.current}º</p>
        </div>
        <div className="min-max-temp">
          <p>{this.props.min}º | {this.props.max}º</p>
        </div>
      </div>
    )
  }
}
