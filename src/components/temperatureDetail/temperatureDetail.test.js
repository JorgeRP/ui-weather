import { renderComponent, expect } from "../../test_helper";
import TemperatureDetail from "./temperatureDetail";

describe("TemperatureDetail", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(TemperatureDetail);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});