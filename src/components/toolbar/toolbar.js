import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './toolbar.css';

export default class Toolbar extends Component {
    render() {
        return(
        <div className="toolbar">        
          <Link to="/" className="btn-toolbar"> Search </Link>
          <Link to="/contact" className="btn-toolbar"> Contact </Link>
          <Link to="/coments" className="btn-toolbar"> Comments </Link>
        </div>
        )
    }
}