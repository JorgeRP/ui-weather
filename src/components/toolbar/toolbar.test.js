import { renderComponentWithRouter, expect } from "../../test_helper";
import Toolbar from "./toolbar";

describe("Toolbar", () => {
  let component;

  beforeEach(() => {
    component = renderComponentWithRouter(Toolbar);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});