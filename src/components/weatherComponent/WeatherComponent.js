import React, { Component } from 'react';
import './WeatherComponent.css';
import SearchBar from '../../containers/searchBar/searchBar';
import Details from '../../containers/details/details';

class WeatherComponent extends Component {
  render() {
    return (
      <div className="WeatherComponent">
        <SearchBar />
        <Details />
      </div>
    );
  }
}

export default WeatherComponent;
