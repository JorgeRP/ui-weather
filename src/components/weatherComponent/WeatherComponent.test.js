import { renderComponent, expect } from "../../test_helper";
import WeatherComponent from "./weatherComponent";

describe("WeatherComponent", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(WeatherComponent);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});
