import React, { Component } from 'react';
import './windDetail.css';

export default class WindDetail extends Component {
  render() {
    return (
      <div>
        <p>Wind speed</p>
        <p className="speed">{this.props.speed} m/s</p>
      </div>
    )
  }
}
