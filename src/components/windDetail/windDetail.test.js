import { renderComponent, expect } from "../../test_helper";
import WindDetail from "./windDetail";

describe("WindDetail", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(WindDetail);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});