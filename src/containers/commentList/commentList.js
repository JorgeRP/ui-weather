import React, { Component } from "react";
import { connect } from "react-redux";
import { getMessages } from "../../actions/contactActions";
import { bindActionCreators } from "redux";
import './commentList.css';

class CommentList extends Component {
  componentWillMount() {
    this.props.getMessages();
  }

  render() {
    if (this.props.contact && this.props.contact.length > 1 && Array.isArray(this.props.contact)) {
      return (
          <div>
              {this.props.contact.map((message) => {
                  return (
                  <div key={message.id} className="container">
                    <p className="content">{message.title}</p>
                    <p className="content">{message.categories}</p>
                    <p className="content">{message.content}</p>
                  </div>
                )
              })}
          </div>
      );
    } else {
      return <div></div>;
    }
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getMessages }, dispatch);
}

function mapStateToProps({ contact }) {
  return { contact };
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentList);
