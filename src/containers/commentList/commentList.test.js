import { renderComponent, expect } from "../../test_helper";
import chai  from "chai";
import spies from 'chai-spies';
import CommentList from "./commentList";

describe("CommentList", () => {
  let component;
  chai.use(spies);

  beforeEach(() => {
    component = renderComponent(CommentList);
    component.props = {
        getMessages: function() {
            
        }
    }
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });

  it('getMessages should be called', () => {
    const spy = chai.spy.on(component.props,"getMessages");  
    expect(spy).to.have.been.called;
    
  })
});