import React, { Component } from 'react';
import { connect } from 'react-redux';
import TemperatureDetail from '../../components/temperatureDetail/temperatureDetail';
import HumidityDetail from '../../components/humidityDetail/humidityDetail';
import WindDetail from '../../components/windDetail/windDetail';
import './details.css'

class Details extends Component {

  render() {
    if (this.props.weather.name) {
      return (
        <div className="details">
          <div className="city-name">{this.props.weather.name}</div>
          <div>
            <TemperatureDetail
              current={this.props.weather.main.temp}
              min={this.props.weather.main.temp_min}
              max={this.props.weather.main.temp_max} />
          </div>
          <div className="line"></div>
          <div>
            <HumidityDetail humidity={this.props.weather.main.humidity} />
          </div>
          <div className="line"></div>
          <div>
            <WindDetail speed={this.props.weather.wind.speed} />
          </div>
        </div>
      )
    } else if (this.props.weather === "undefined") {
      return (
        <div className="search-error">
          <p>City not found</p>
        </div>
      )
    } else {
      return null;
    }
  }
}

function mapStateToProps({ weather }) {
  return { weather };
}

export default connect(mapStateToProps)(Details)
