import { renderComponent, expect } from "../../test_helper";
import DetailList from "./details";

describe("DetailList", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(DetailList);
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });
});