import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { postMessage } from '../../actions/contactActions';
import './postForm.css';

export class PostForm extends Component {
  constructor() {
    super();
    this.formWasSent = false;
    this.renderField = this.renderField.bind(this);
    this.renderTextarea = this.renderTextarea.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  renderField(field) {
    return (
      <div>
        <input
          className={field.meta.touched && this.formWasSent && field.meta.error ? "input-error" : "custom-input"}
          type="text"
          placeholder={field.placeholder}
          {...field.input}
        />
        <p className="form-error">{field.meta.touched && this.formWasSent ? field.meta.error : ''}</p>
      </div>
    )
  }

  renderTextarea(field) {
    return (
      <div>
        <textarea
          className={field.meta.touched && this.formWasSent && field.meta.error ? "input-error-textarea" : "custom-textbox"}
          type="text"
          placeholder={field.placeholder}
          {...field.input}
        />
        <p className="form-error">{field.meta.touched && this.formWasSent ? field.meta.error : ''}</p>
      </div>
    )
  }

  onSubmit(values) {
    this.props.postMessage(values, this.resetForm);
  }

  resetForm() {
    //resets form
    this.props.reset();
  }

  render() {

    const { handleSubmit } = this.props; //handleSubmit is inside props because we are binding the PostForm class to reduxForm
    return (
      <form onSubmit={handleSubmit(this.onSubmit)}>
        <Field
          placeholder="Insert your name"
          name="title"
          component={this.renderField}
        />
        <Field
          placeholder="Insert your email"
          name="categories"
          component={this.renderField}
        />
        <Field
          placeholder="Send your message"
          name="content"
          component={this.renderTextarea}
        />
        <button className="btn-submit" type="submit" onClick={() => this.formWasSent = true}>Send</button>
      </form>
    )
  }
}

function validate(values) {
  let errors = {};

  if (!values.title) {
    errors.title = 'You should enter your name';
  }

  if (!values.categories) {
    errors.categories = 'You should enter your email';
  }

  if (!values.content) {
    errors.content = 'You should enter a message';
  }

  return errors;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ postMessage }, dispatch);
}

function mapStateToProps({ contact }) {
  return { contact };
}
//adds a ton of properties to this.props by binding ReduxForm to PostForm
export default reduxForm({
  validate,
  form: "postNewForm"
})(connect(mapStateToProps, mapDispatchToProps)(PostForm))
