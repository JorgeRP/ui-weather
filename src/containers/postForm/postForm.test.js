import { renderComponent, expect } from "../../test_helper";
import { postMessage } from "../../actions/contactActions";
import chai  from "chai";
import spies from 'chai-spies';
import {PostForm} from "./postForm";

describe("PostForm", () => {
  let component;
  chai.use(spies);

  beforeEach(() => {
    component = new PostForm();
    component.props = {
      postMessage: postMessage,

      reset: function() {

      }
    }
  });

  it("component should exists", () => {
    expect(component).to.exist;
  });

  it("should call renderField and renderTextarea", () => {
    const spy = chai.spy.on(component, "renderField");
    const spy2 = chai.spy.on(component, "renderTextarea");

    expect(spy).to.have.been.called;
    expect(spy2).to.have.been.called;
  });

  it("should call postMessage on submit", () => {
    const spy = chai.spy.on(component.props, "postMessage");
    component.onSubmit();
    expect(spy).to.have.been.called;
  });

  it("should call reset on resetForm", () => {
    const spy = chai.spy.on(component.props, "reset");
    component.resetForm();
    expect(spy).to.have.been.called;
  });
  
});