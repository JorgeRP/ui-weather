import React, { Component } from 'react';
import { fetchWeather } from '../../actions/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './searchBar.css';


export class SearchBar extends Component {

  constructor(props) {
    super(props);
    this.state = { term: '' };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.focusInput = this.focusInput.bind(this);
  }

  componentDidMount() {
    this.inputName.focus();
  }

  focusInput() {
    this.inputName.focus();
  }

  handleChange(event) {
    this.setState({ term: event.target.value })
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.fetchWeather(this.state.term);
    this.setState({ term: '' });
  }
  render() {
    return (
      <div className="search-bar">
        <div>
          <fieldset>
            <form onSubmit={this.onSubmit}>
              <input ref={(input) => { this.inputName = input }}
                className="input-field"
                placeholder="Type a city name and press enter"
                value={this.state.term}
                onChange={this.handleChange}
                onBlur={this.focusInput} />
              <button className="btn-searchBar">Submit</button>
            </form>
          </fieldset>
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar)
