import { expect } from "../../test_helper";
import { fetchWeather } from "../../actions/index";
import chai  from "chai";
import spies from 'chai-spies';
import { SearchBar } from './searchBar';

describe("SearchBar", () => {
    const component = new SearchBar();
    chai.use(spies);

    component.inputName = {
        focus: function() {

        }
    }

    component.props = {
        fetchWeather: fetchWeather
    }

    const event = {
        preventDefault: function() {
            
        }
    }

    it("component should exists", () => {
        expect(component).to.exist;
    });

    it("should call input focus", () => {
        const spy = chai.spy.on(component.inputName, "focus");
        expect(spy).to.have.been.called;
    });

    it("should call fetchWeather on submit", () => {
        const spy = chai.spy.on(component.props, "fetchWeather");
        component.onSubmit(event);
        expect(spy).to.have.been.called;
    })
})