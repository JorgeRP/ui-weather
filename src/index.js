import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./components/app/app";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router } from "react-router-dom";
import reducers from "./reducers";
import createHistory from "history/createBrowserHistory";
import Async from "./middlewares/async";
const createStoreWithMiddleware = applyMiddleware(Async)(createStore);
const history = createHistory();

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={history}>
      <div>
        <App />
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
