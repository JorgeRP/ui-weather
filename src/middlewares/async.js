export default function Async({ dispatch }) {
  return next => action => {
    if (!action.payload || !action.payload.then) {
      return next(action);
    }

    action.payload.then(response => {
      action.payload = response;
      return next(action);
    });
  };
}
