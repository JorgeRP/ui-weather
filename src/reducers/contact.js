import { POST_MESSAGE, GET_MESSAGE } from '../actions/contactActions.js';

export default function (state = [], action) {
  switch (action.type) {
    case POST_MESSAGE:
      return action.payload.data ? action.payload.data : 'undefined';

    case GET_MESSAGE:
      return action.payload.data ? action.payload.data : 'undefined';

    default:
      return state
  }
}
