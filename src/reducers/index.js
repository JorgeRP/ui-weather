import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import weather from './weather';
import contact from './contact';

export default combineReducers({
  weather,
  contact,
  form: formReducer
});
