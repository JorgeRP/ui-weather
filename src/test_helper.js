import _$ from 'jquery';
import React from "react";
import ReactDOM from "react-dom";
import TestUtils from "react-addons-test-utils";
import jsdom from "jsdom";
import chai, { expect } from "chai";
import spies from 'chai-spies';
import { Provider } from "react-redux";
import { Router } from 'react-router-dom';
import { createStore } from "redux";
import reducers from "../src/reducers";
import createHistory from "history/createMemoryHistory";

const history = createHistory()

global.document = jsdom.jsdom("<!doctype html><html><body></body></html>");
global.window = global.document.defaultView;
global.navigator = global.window.navigator;

const $ = _$(window);


function renderComponent(ComponentClass, props = {}, state = {}) {
  const componentInstance = TestUtils.renderIntoDocument(
    <Provider store={createStore(reducers, state)}>
      <ComponentClass {...props} />
    </Provider>
  );

  return $(ReactDOM.findDOMNode(componentInstance));
}

function renderComponentWithRouter(ComponentClass, props ={}, state ={}) {
  const componentInstance = TestUtils.renderIntoDocument(
    <Provider store={createStore(reducers, state)}>
      <Router history = {history} >
        <ComponentClass {...props} />
      </Router>
    </Provider>
  );

  return $(ReactDOM.findDOMNode(componentInstance));
}

export { renderComponent, renderComponentWithRouter, expect };
